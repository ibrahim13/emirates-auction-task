package com.emiratesauction.emiratesaucation.api.response;

import com.emiratesauction.emiratesaucation.api.APIConstants;
import com.google.gson.annotations.SerializedName;

public class AuctionInfo {

    @SerializedName(APIConstants.KEY_BIDS)
    private int bids;

    @SerializedName(APIConstants.KEY_END_DATE)
    private long endDate;

    @SerializedName(APIConstants.KEY_END_DATE_EN)
    private String endDateEn;

    @SerializedName(APIConstants.KEY_END_DATE_AR)
    private String endDateAr;

    @SerializedName(APIConstants.KEY_CURRENCY_EN)
    private String currencyEn;

    @SerializedName(APIConstants.KEY_CURRENCY_AR)
    private String currencyAr;

    @SerializedName(APIConstants.KEY_CURRENT_PRICE)
    private int currentPrice;

    @SerializedName(APIConstants.KEY_LOT)
    private int lot;

    public AuctionInfo() {
    }

    public int getBids() {
        return bids;
    }

    public void setBids(int bids) {
        this.bids = bids;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public String getEndDateEn() {
        return endDateEn;
    }

    public void setEndDateEn(String endDateEn) {
        this.endDateEn = endDateEn;
    }

    public String getEndDateAr() {
        return endDateAr;
    }

    public void setEndDateAr(String endDateAr) {
        this.endDateAr = endDateAr;
    }

    public String getCurrencyEn() {
        return currencyEn;
    }

    public void setCurrencyEn(String currencyEn) {
        this.currencyEn = currencyEn;
    }

    public String getCurrencyAr() {
        return currencyAr;
    }

    public void setCurrencyAr(String currencyAr) {
        this.currencyAr = currencyAr;
    }

    public int getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(int currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getLot() {
        return lot;
    }

    public void setLot(int lot) {
        this.lot = lot;
    }
}