package com.emiratesauction.emiratesaucation.api.response;

import com.emiratesauction.emiratesaucation.api.APIConstants;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OnlineCarsResponse {

    @SerializedName(APIConstants.KEY_REFRESH_INTERVAL)
    private int refreshInterval;

    @SerializedName(APIConstants.KEY_TICKS)
    private Long ticks;

    @SerializedName(APIConstants.KEY_CARS)
    private List<Car> cars;

    public OnlineCarsResponse() {
    }

    public int getRefreshInterval() {
        return refreshInterval;
    }

    public Long getTicks() {
        return ticks;
    }

    public List<Car> getCars() {
        return cars;
    }
}
