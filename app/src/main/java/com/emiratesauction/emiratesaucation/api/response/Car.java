package com.emiratesauction.emiratesaucation.api.response;

import com.emiratesauction.emiratesaucation.api.APIConstants;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Car {

    @SerializedName(APIConstants.KEY_CAR_ID)
    private long carId;

    @SerializedName(APIConstants.KEY_IMAGE)
    private String image;

    @SerializedName(APIConstants.KEY_YEAR)
    private int year;

    @SerializedName(APIConstants.KEY_MAKE_EN)
    private String makeEn;

    @SerializedName(APIConstants.KEY_MAKE_AR)
    private String makeAr;

    @SerializedName(APIConstants.KEY_MODEL_EN)
    private String modelEn;

    @SerializedName(APIConstants.KEY_MODEL_AR)
    private String modelAr;

    @SerializedName(APIConstants.KEY_AUCTION_INFO)
    private AuctionInfo auctionInfo;

    public Car() {
    }

    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMakeEn() {
        return makeEn;
    }

    public void setMakeEn(String makeEn) {
        this.makeEn = makeEn;
    }

    public String getMakeAr() {
        return makeAr;
    }

    public void setMakeAr(String makeAr) {
        this.makeAr = makeAr;
    }

    public String getModelEn() {
        return modelEn;
    }

    public void setModelEn(String modelEn) {
        this.modelEn = modelEn;
    }

    public String getModelAr() {
        return modelAr;
    }

    public void setModelAr(String modelAr) {
        this.modelAr = modelAr;
    }

    public AuctionInfo getAuctionInfo() {
        return auctionInfo;
    }

    public void setAuctionInfo(AuctionInfo auctionInfo) {
        this.auctionInfo = auctionInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return getCarId() == car.getCarId();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getCarId());
    }
}
