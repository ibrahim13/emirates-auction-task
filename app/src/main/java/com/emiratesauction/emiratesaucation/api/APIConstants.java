package com.emiratesauction.emiratesaucation.api;

public class APIConstants {

    public static final String BASE_URL = "http://api.emiratesauction.com/v2/";
    public static final String CARS_ONLINE_URL = "carsonline";

    public static final String KEY_REFRESH_INTERVAL = "RefreshInterval";
    public static final String KEY_TICKS = "Ticks";
    public static final String KEY_CARS = "Cars";

    public static final String KEY_CAR_ID = "carID";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_YEAR = "year";
    public static final String KEY_MAKE_EN = "makeEn";
    public static final String KEY_MAKE_AR = "makeAr";
    public static final String KEY_MODEL_EN = "modelEn";
    public static final String KEY_MODEL_AR = "modelAr";
    public static final String KEY_AUCTION_INFO = "AuctionInfo";
    public static final String KEY_BIDS = "bids";
    public static final String KEY_END_DATE = "endDate";
    public static final String KEY_CURRENCY_EN = "currencyEn";
    public static final String KEY_CURRENCY_AR = "currencyAr";
    public static final String KEY_CURRENT_PRICE = "currentPrice";
    public static final String KEY_LOT = "lot";
    public static final String KEY_END_DATE_EN = "endDateEn";
    public static final String KEY_END_DATE_AR = "endDateAr";
}
