package com.emiratesauction.emiratesaucation.api;

import com.emiratesauction.emiratesaucation.api.response.OnlineCarsResponse;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CarsService {
    @GET(APIConstants.CARS_ONLINE_URL)
    Single<Response<OnlineCarsResponse>> getOnlineCars(@Query("ticks") Long ticks);
}
