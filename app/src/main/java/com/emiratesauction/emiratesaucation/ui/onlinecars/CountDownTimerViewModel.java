package com.emiratesauction.emiratesaucation.ui.onlinecars;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CountDownTimerViewModel extends ViewModel {

    private final CompositeDisposable disposables = new CompositeDisposable();
    private long period = 60 * 1;
    
    private MutableLiveData<Long> mReminderTime = new MutableLiveData<>();

    @Inject
    public CountDownTimerViewModel() {
    }

    public void setPeriod(long period) {
        this.period = period;
    }

    public void stopTimer() {

        disposables.clear();

    }

    public void startTimer() {

        disposables.add(Observable.interval(0, 1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    long reminder = period - aLong;
                    mReminderTime.postValue(reminder);
                })
        );

    }

    public MutableLiveData<Long> getReminderTime() {
        return mReminderTime;
    }

    @Override
    protected void onCleared() {
        super.onCleared();

        stopTimer();

    }


    public void restartTimer() {
        stopTimer();
        startTimer();
    }
}
