package com.emiratesauction.emiratesaucation.ui.onlinecars;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.emiratesauction.emiratesaucation.R;
import com.emiratesauction.emiratesaucation.api.response.Car;
import com.emiratesauction.emiratesaucation.util.Util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OnlineCarsAdapter extends Adapter<ViewHolder> {

    private static final int HEADER_VIEW_TYPE = 0;
    private static final int ITEM_VIEW_TYPE = 1;

    private Context mContext;

    private List<Car> items;

    public OnlineCarsAdapter(Context context) {
        this.mContext = context;
        this.items = Collections.emptyList();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_VIEW_TYPE;
        } else {
            return ITEM_VIEW_TYPE;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == HEADER_VIEW_TYPE) {

            View view = LayoutInflater.from(mContext).inflate(R.layout.header, parent, false);

            return new HeaderViewHolder(view);
        } else {

            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_online_car, parent, false);

            return new CarViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (holder instanceof CarViewHolder) {

            CarViewHolder carViewHolder = (CarViewHolder) holder;

            Car car = items.get(holder.getAdapterPosition());

            String imageURL = car.getImage().replace("[w]", "0").replace("[h]", "0");

            Glide.with(mContext).load(imageURL).into(carViewHolder.ivCarImage);

            String make = Util.isRTL() ? car.getMakeAr() : car.getMakeEn();

            String model = Util.isRTL() ? car.getModelAr() : car.getModelEn();

            String title = make.concat(" ").concat(model).concat(" ").concat(String.valueOf(car.getYear()));

            carViewHolder.tvTitle.setText(title);

            DecimalFormat formatter = new DecimalFormat("#,###,###");

            String price = formatter.format(car.getAuctionInfo().getCurrentPrice());

            carViewHolder.tvPrice.setText(price);

            String currency = Util.isRTL() ? car.getAuctionInfo().getCurrencyAr() : car.getAuctionInfo().getCurrencyEn();

            carViewHolder.tvCurrency.setText(currency);

            carViewHolder.tvLot.setText(String.valueOf(car.getAuctionInfo().getLot()));

            carViewHolder.tvBids.setText(String.valueOf(car.getAuctionInfo().getBids()));

            try {
                String timeLeft = Util.getTimeLeftFormatted(Util.getDateFormatted(car.getAuctionInfo().getEndDateEn()), new Date());
                int timeLeftInMinutes = Util.getTimeLeftInMinutes(Util.getDateFormatted(car.getAuctionInfo().getEndDateEn()), new Date());
                if (timeLeftInMinutes < 5) {
                    carViewHolder.tvTimeLeft.setTextColor(mContext.getResources().getColor(R.color.red));
                } else {
                    carViewHolder.tvTimeLeft.setTextColor(mContext.getResources().getColor(R.color.textDarkPrimary));
                }
                carViewHolder.tvTimeLeft.setText(timeLeft);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public int getItemCount() {
        if (items.isEmpty()) {
            return items.size();
        } else {
            return items.size() + 1;
        }
    }

    public void setItems(List<Car> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    class HeaderViewHolder extends ViewHolder {

        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    class CarViewHolder extends ViewHolder {

        @BindView(R.id.iv_car_image)
        ImageView ivCarImage;

        @BindView(R.id.tv_title)
        AppCompatTextView tvTitle;

        @BindView(R.id.tv_price)
        AppCompatTextView tvPrice;

        @BindView(R.id.tv_currency)
        AppCompatTextView tvCurrency;

        @BindView(R.id.tv_lot)
        AppCompatTextView tvLot;

        @BindView(R.id.tv_bids)
        AppCompatTextView tvBids;

        @BindView(R.id.tv_time_left)
        AppCompatTextView tvTimeLeft;

        public CarViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
