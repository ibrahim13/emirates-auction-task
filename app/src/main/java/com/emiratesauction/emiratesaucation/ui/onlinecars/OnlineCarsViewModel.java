package com.emiratesauction.emiratesaucation.ui.onlinecars;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.emiratesauction.emiratesaucation.api.response.Car;
import com.emiratesauction.emiratesaucation.api.response.OnlineCarsResponse;
import com.emiratesauction.emiratesaucation.repository.OnlineCarsRepository;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;
import retrofit2.Response;

public class OnlineCarsViewModel extends ViewModel {

    private OnlineCarsRepository onlineCarsRepository;

    private MutableLiveData<OnlineCarsResponse> responseLiveData = new MutableLiveData<>();

    private MutableLiveData<List<Car>> carsLiveData = new MutableLiveData<>();

    private MutableLiveData<Boolean> showLoadingLiveData = new MutableLiveData<>();

    @Inject
    public OnlineCarsViewModel(OnlineCarsRepository onlineCarsRepository) {
        this.onlineCarsRepository = onlineCarsRepository;
    }

    public void getOnlineCars() {

        showLoadingLiveData.postValue(true);

        onlineCarsRepository.getOnlineCars(null, new Consumer<Response<OnlineCarsResponse>>() {
            @Override
            public void accept(Response<OnlineCarsResponse> response) throws Exception {

                if (response.isSuccessful()) {

                    OnlineCarsResponse carsResponse = response.body();

                    if (carsResponse != null) {

                        responseLiveData.postValue(carsResponse);

                        carsLiveData.postValue(carsResponse.getCars());

                    }

                }

                showLoadingLiveData.postValue(false);

            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                showLoadingLiveData.postValue(false);
            }
        });

    }

    public void refreshOnlineCars() {

        OnlineCarsResponse carsResponse = responseLiveData.getValue();

        Long ticks = null;

        if (carsResponse != null) {

            ticks = carsResponse.getTicks();

        }

        onlineCarsRepository.getOnlineCars(ticks, new Consumer<Response<OnlineCarsResponse>>() {
            @Override
            public void accept(Response<OnlineCarsResponse> response) throws Exception {

                if (response.isSuccessful()) {

                    OnlineCarsResponse carsResponse = response.body();

                    if (carsResponse != null) {

                        responseLiveData.postValue(carsResponse);

                        List<Car> newCarsList = carsResponse.getCars();

                        List<Car> carsList = carsLiveData.getValue();

                        if (carsList == null) {

                            carsList = Collections.emptyList();

                        }

                        if (newCarsList != null && newCarsList.size() > 0) {

//                            Collections.reverse(newCarsList);

//                            for (Car car: newCarsList) {
//
//                                if (!carsList.contains(car)) {
//                                    carsList.add(0, car);
//                                }
//
//                            }

                            carsList.addAll(0, newCarsList);

                        }

                        carsLiveData.postValue(carsList);

                    }

                }

            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {

            }
        });

    }

    public MutableLiveData<List<Car>> getCarsLiveData() {
        return carsLiveData;
    }

    public MutableLiveData<Boolean> getShowLoadingLiveData() {
        return showLoadingLiveData;
    }
}
