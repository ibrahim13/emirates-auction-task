package com.emiratesauction.emiratesaucation.ui.onlinecars;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.emiratesauction.emiratesaucation.R;
import com.emiratesauction.emiratesaucation.api.response.Car;
import com.emiratesauction.emiratesaucation.api.response.OnlineCarsResponse;
import com.emiratesauction.emiratesaucation.di.Injectable;
import com.emiratesauction.emiratesaucation.util.SpaceItemDecoration;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OnlineCarsFragment extends Fragment implements Injectable, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    @BindView(R.id.swipe_to_refresh_cars)
    SwipeRefreshLayout swipeToRefreshCars;
    @BindView(R.id.fl_loading_view)
    FrameLayout flLoadingView;
    @BindView(R.id.rv_cars)
    RecyclerView rvCars;
    private OnlineCarsViewModel mViewModel;
    private CountDownTimerViewModel countDownTimerViewModel;
    private Context mContext;
    private View mContentView;
    private OnlineCarsAdapter carsAdapter;

    public static OnlineCarsFragment newInstance() {
        return new OnlineCarsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        carsAdapter = new OnlineCarsAdapter(mContext);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mContentView = inflater.inflate(R.layout.fragment_online_cars, container, false);

        ButterKnife.bind(this, mContentView);

        return mContentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeToRefreshCars.setOnRefreshListener(this);

        rvCars.setLayoutManager(new LinearLayoutManager(mContext));

        rvCars.setHasFixedSize(true);

        rvCars.setAdapter(carsAdapter);

        int topBottomSpace = (int) getResources().getDimension(R.dimen.top_bottom_item_space);

        int leftRightSpace = (int) getResources().getDimension(R.dimen.left_right_item_space);

        rvCars.addItemDecoration(new SpaceItemDecoration(topBottomSpace, leftRightSpace));

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        countDownTimerViewModel = ViewModelProviders.of(this, mViewModelFactory).get(CountDownTimerViewModel.class);

        countDownTimerViewModel.getReminderTime().observe(this, new Observer<Long>() {
            @Override
            public void onChanged(@Nullable Long reminder) {
                if (reminder != null && reminder < 1) {
                    swipeToRefreshCars.setRefreshing(true);
                    mViewModel.refreshOnlineCars();
                }
            }
        });

        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(OnlineCarsViewModel.class);

        mViewModel.getShowLoadingLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                if (show != null) {
                    flLoadingView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            }
        });

        mViewModel.getCarsLiveData().observe(this, new Observer<List<Car>>() {
            @Override
            public void onChanged(@Nullable List<Car> cars) {

                countDownTimerViewModel.restartTimer();

                if (cars != null) {

                    carsAdapter.setItems(cars);

                }

                swipeToRefreshCars.setRefreshing(false);

            }
        });

        if (savedInstanceState == null) {
            mViewModel.getOnlineCars();
        }

    }

    @Override
    public void onRefresh() {
        mViewModel.refreshOnlineCars();
    }
}
