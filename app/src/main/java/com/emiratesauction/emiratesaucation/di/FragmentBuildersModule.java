package com.emiratesauction.emiratesaucation.di;

import com.emiratesauction.emiratesaucation.ui.onlinecars.OnlineCarsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract OnlineCarsFragment contributeRepoFragment();
}
