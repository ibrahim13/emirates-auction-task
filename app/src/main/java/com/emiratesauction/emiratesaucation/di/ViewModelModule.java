package com.emiratesauction.emiratesaucation.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.emiratesauction.emiratesaucation.ui.onlinecars.CountDownTimerViewModel;
import com.emiratesauction.emiratesaucation.ui.onlinecars.OnlineCarsViewModel;
import com.emiratesauction.emiratesaucation.viewmodel.AppViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


@Module
abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(AppViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(OnlineCarsViewModel.class)
    abstract ViewModel bindOnlineCarsViewModel(OnlineCarsViewModel onlineCarsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CountDownTimerViewModel.class)
    abstract ViewModel bindLiveDataTimerViewModel(CountDownTimerViewModel countDownTimerViewModel);

}
