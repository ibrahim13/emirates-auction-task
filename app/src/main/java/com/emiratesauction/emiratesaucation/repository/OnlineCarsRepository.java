package com.emiratesauction.emiratesaucation.repository;

import android.annotation.SuppressLint;

import com.emiratesauction.emiratesaucation.api.CarsService;
import com.emiratesauction.emiratesaucation.api.response.OnlineCarsResponse;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

@Singleton
public class OnlineCarsRepository {

    private CarsService carsService;

    @Inject
    public OnlineCarsRepository(CarsService carsService) {
        this.carsService = carsService;
    }

    @SuppressLint("CheckResult")
    public void getOnlineCars(Long ticks, Consumer<Response<OnlineCarsResponse>> successConsumer,
                              Consumer<Throwable> errorConsumer) {

        carsService.getOnlineCars(ticks)
                .subscribeOn(Schedulers.io())
                .subscribe(successConsumer, errorConsumer);

    }

}
