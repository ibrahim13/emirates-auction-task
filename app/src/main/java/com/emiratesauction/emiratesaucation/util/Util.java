package com.emiratesauction.emiratesaucation.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Util {

    public static final String SERVER_DATE_FORMAT = "dd MMM hh:mm aa";

    public static boolean isRTL() {
        return isRTL(Locale.getDefault());
    }

    public static boolean isRTL(Locale locale) {
        final int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
        return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT ||
                directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
    }

    public static int getTimeLeftInMinutes(Date startDate, Date endDate) {

        int elapsedSeconds = getElapsedTimeInSeconds(startDate, endDate);

        return elapsedSeconds / 60;
    }

    public static String getTimeLeftFormatted(Date startDate, Date endDate) {

        int elapsedSeconds = getElapsedTimeInSeconds(startDate, endDate);

        int hours = elapsedSeconds / 3600;
        int minutes = (elapsedSeconds % 3600) / 60;
        int seconds = elapsedSeconds % 60;

        return String.format(Locale.ENGLISH,"%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static Date getDateFormatted(String dateInServerFormat) throws ParseException {
        Date date = new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.ENGLISH).parse(dateInServerFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        // Workaround to set year because the date that returned from server does not contains year.
        calendar.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));
        return calendar.getTime();
    }


    private static int getElapsedTimeInSeconds(Date start, Date end) {
        return (int) (start.getTime() - end.getTime()) / (1000);
    }
}
