package com.emiratesauction.emiratesaucation.util;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

    private int topBottomSpace;
    private int leftRightSpace;

    public SpaceItemDecoration(int topBottomSpace, int leftRightSpace) {
        this.topBottomSpace = topBottomSpace;
        this.leftRightSpace = leftRightSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.top = topBottomSpace;
        outRect.bottom = topBottomSpace;
        outRect.left = leftRightSpace;
        outRect.right = leftRightSpace;
    }
}
